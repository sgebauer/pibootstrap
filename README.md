pibootstrap
===========

Debian image build tool for the RaspberryPi 2/3  
Usage: `pibootstrap <image>`

What it does
------------

pibootstrap creates a clean, minimal and up-to-date RaspberryPi Debian image
from scratch, including:
  * A btrfs root filesystem with transparnt compression by default
  * A proper kernel / bootloader setup that doesn't break on every kernel update
  * Preinstalled WiFi firmware
  * Preinstalled OpenSSH server with a configurable pre-installed authorized
    key for root
  * Systemd networkd enabled and configured for DHCP

How to use
----------

  1. `cp config.example config`
  2. Change settings in `config` to your needs
  3. `sudo ./pibootstrap <image>`
  4. (Optional) Go get a coffee. Bootstraping a foreign architecture system
     takes a while ;)
  5. When pibootstrap is finnished, you can flash the output image to an SD
     card. Don't forget to resize the root partition!
  6. (Optional) Add the ssh_pubkey file that was created alongside the
     output image to your known_hosts.

Dependencies
------------

pibootstrap requires `fdisk`, `mkfs.btrfs`, `debootstrap`, `wget`,
`qemu-arm-static` and binfmt-registration for `qemu-arm-static`.

Technical details
-----------------

pibootstrap fixes some kernel/bootloader/firmware bugs at build time while
installing as little custom/non-packaged software as possible.

**Kernel/Bootloader**:  
As of the time of writing, neither Debian nor Raspbian nor the RaspberryPi
Foundation have a packaged kernel and bootloader that actually works without the
need to change `/boot/config.txt` after every kernel or initramfs update.
So pibootstrap simply uses a DIY setup here:  
`resources/updateboot` makes sure that `/boot/kernel.img` and `/boot/initrd.img`
are always copies (FAT doesn't support symlinks) of the latest kernel and initrd
images. To do this, updateboot must be called whenever the kernel or initramfs
is updated. This is done by creating symlinks in the appropriate handler
directories (e.g. `/etc/kernel/postinst.d`) and installing Raspbian's kernel,
since the RaspberryPi Foundation's kernel package does not run
postinst.d/postrm.d scripts.  
The default config.txt (`resources/config.txt`) then tells the bootloader to
load `/boot/kernel.img` and `/boot/initrd.img`.

**WiFi/Bluetooth**:  
As of the time of writing, the WiFi firmware package from Debian/Raspbian is
broken, so pibootstrap installs it from the RaspberryPi Foundation's repo
instead.
